create database euroTruck;
use euroTruck;
create table Empresa(
    id int not null auto_increment,
    nome varchar(45) not null default 'DEFAULT',
    senha varchar(15) not null default '123',
    margemDeLucro float not null default 0.0,
    lucroTotalDaEmpresa double not null default 0.0,
    primary key(id)
)default charset=utf8;

create table Veiculo(
	Empresa_id int not null,
    placa char(7) not null,
	tipo varchar(8)not null,
	habilitadoParaUso boolean not null,
    foreign key(Empresa_id) references Empresa(id),
	primary key(placa)
)default charset=utf8;

create table Frete(
	idFrete int not null auto_increment,
    Veiculo_placa varchar(45)not null,
    pesoCarga float not null,
    distancia float not null,
    tempoRealHoras float not null,
    precoFinal float not null,
    nomeCliente varchar(45) not null,
    statusFrete boolean not null,
    foreign key(Veiculo_placa) references Veiculo(placa),
    primary key(idFrete)
)default charset=utf8;

insert into Empresa (nome,margemDeLucro,lucroTotalDaEmpresa) values('DEFAULT',0.0,0.0);
