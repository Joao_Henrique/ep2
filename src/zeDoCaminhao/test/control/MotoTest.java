/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author hellsank
 */
public class MotoTest {
    /*MotoTest(){
        iniciaVariaveis();
        testCalculoPrecoMelhorCombustivel();
        motoFazEntrega();
    }*/
    /**
     * Test of calculoPrecoMelhorCombustivel method, of class Moto.
     */
    private float distancia;
    private float pesoCarga;
    private int tempoMax;
    private Moto instance ;
    @Before
    public void iniciaVariaveis(){
        distancia = 110.00f;
        pesoCarga = 30f;
        tempoMax = 1;
        instance = new Moto();
    }
            
    @Test
    public void testCalculoPrecoMelhorCombustivel() {
        //System.out.println("calculoPrecoMelhorCombustivel");
        
        float result = instance.calculoPrecoMelhorCombustivel(distancia, pesoCarga);
        float expResultPreco = 11.936f;
        
        assertEquals(expResultPreco, result, 0.1);
        
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void motoFazEntrega(){
        boolean motoFazEntrega = instance.checaVeiculoFazEntrega(distancia, tempoMax, pesoCarga);
        boolean expResultFazEntrega = true;
        assertEquals(expResultFazEntrega,motoFazEntrega);
    }
    
}
