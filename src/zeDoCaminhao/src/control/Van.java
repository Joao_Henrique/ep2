/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

/**
 *
 * @author hellsank
 */
public class Van extends Veiculo{
    final short DIESEL = 0;
    Van(String placa,boolean habilitadoParaUso){
        this();
        this.setPlaca(placa);
        this.setHabilitadoParaUso(habilitadoParaUso);
    }
    public Van(){
        this.cargaMaxima = 3500;
        this.velocidadeMedia = 80;
        float [] aux = {10f,0.001f};
        this.combustivelRendimentoReducaoRendimento.add(aux);
        this.setHabilitadoParaUso(true);
    }
    @Override
    public float calculoPrecoMelhorCombustivel(float distancia, float pesoCarga) {
        return calculoPrecoCombustivel(DIESEL,"DIESEL",distancia,pesoCarga);
    }
    
}
