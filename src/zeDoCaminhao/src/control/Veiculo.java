/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import model.Conector;
import model.Crud;

/**
 *
 * @author hellsank
 */
public class Veiculo {
    private  final float PrecoAlcool = 3.499f;
    private  final float PrecoGasolina = 4.449f;
    private final float PrecoDIESEL = 3.869f;
    protected float getPreco(String combustivel){
        if(combustivel.equals("DIESEL")){
            return PrecoDIESEL;
        }else if(combustivel.equals("GASOLINA")){
            return PrecoGasolina;
        }else{
            return PrecoAlcool;
        }
    }
    //Constantes
    protected String placa;
    protected String tipo;
    protected float velocidadeMedia;
    protected float cargaMaxima;
    protected float cargaAtual;
    protected boolean habilitadoParaUso;
    protected ArrayList<float [] > combustivelRendimentoReducaoRendimento = new ArrayList<>();
    private Conector con = new Conector();
    //Atributos
    public float getVelocidadeMedia() {
        return velocidadeMedia;
    }

    public void setVelocidadeMedia(float velocidadeMedia) {
        this.velocidadeMedia = velocidadeMedia;
    }

    public float getCargaAtual() {
        return cargaAtual;
    }

    public void setCargaAtual(float cargaAtual) {
        this.cargaAtual = cargaAtual;
    }
    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getCargaMaxima() {
        return cargaMaxima;
    }

    public void setCargaMaxima(float cargaMaxima) {
        this.cargaMaxima = cargaMaxima;
    }

    public boolean isHabilitadoParaUso() {
        return habilitadoParaUso;
    }

    public void setHabilitadoParaUso(boolean habilitadoParaUso) {
        this.habilitadoParaUso = habilitadoParaUso;
    }

    

    public Veiculo(String placa, String tipo, boolean habilitadoParaUso) {
        this.placa = placa;
        this.tipo = tipo;
        this.habilitadoParaUso = habilitadoParaUso;
    }
    public Veiculo(){
        
    }
    //Getter && Setter && Construtor
    public  float calculoPrecoMelhorCombustivel(float distancia, float pesoCarga){
        return 9999999999999f;
    }
    //Metodos Abstratos
    public boolean cargaPassaLimite(float cargaDesejada){//retorna true se passa
        return cargaDesejada>this.cargaMaxima;
    }
    public float calculoPrecoCombustivel(short idCombustivel,String nomeCombustivel, float distancia, float pesoCarga){
        float rendimentoReal = this.combustivelRendimentoReducaoRendimento.get(idCombustivel)[0] - pesoCarga*this.combustivelRendimentoReducaoRendimento.get(idCombustivel)[1];
        float preco = (this.getPreco(nomeCombustivel)/rendimentoReal)*distancia;
        return preco;
    }
    
    public float calculoTempoMinimo(float distancia){
        float tempoMinimo = distancia/this.velocidadeMedia;
        return tempoMinimo;
    }
    public boolean checaVeiculoFazEntrega(float distancia ,int tempoHoras, float pesoCarga){
        if((calculoTempoMinimo(distancia)<=tempoHoras) && !cargaPassaLimite(pesoCarga))
            return true;
        return (false);
    }
    //Outros Metodos
    
    public ArrayList<Veiculo> pegaVeiculosNoBanco() throws SQLException{
        ArrayList<Veiculo> veiculos = new ArrayList<>();
        try{
            ResultSet veiculosBanco = con.select("select * from Veiculo;");
            while(veiculosBanco.next()){
                String placa = veiculosBanco.getString("placa");
                String tipo = veiculosBanco.getString("tipo");
                boolean habilitadoParaUso = veiculosBanco.getBoolean("habilitadoParaUso");
                veiculos.add(new Veiculo(placa,tipo,habilitadoParaUso));
            }
            return veiculos;
        }catch(SQLException ex){
            System.out.println("Error "+ex);
            return null;
        }
    }
     

}
