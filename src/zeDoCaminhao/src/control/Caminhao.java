/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javafx.util.Pair;

/**
 *
 * @author hellsank
 */
public class Caminhao extends Veiculo{
    final short DIESEL = 0;
    Caminhao(String placa,boolean habilitadoParaUso){
        this();
        this.setPlaca(placa);
        this.setHabilitadoParaUso(habilitadoParaUso);
    }
    public Caminhao(){
        this.cargaMaxima = 30000;
        this.velocidadeMedia = 60;
        float [] aux = {8f,0.0002f};
        this.combustivelRendimentoReducaoRendimento.add(aux);
        this.setHabilitadoParaUso(true);
    }
    @Override
    public float calculoPrecoMelhorCombustivel(float distancia, float pesoCarga) {
        return calculoPrecoCombustivel(DIESEL,"DIESEL",distancia,pesoCarga);
    }
    
}
