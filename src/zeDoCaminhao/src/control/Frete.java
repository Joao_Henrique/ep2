/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Conector;

/**
 *
 * @author hellsank
 */
public class Frete {
    private int idFrete;
    private float pesoCarga;
    private float distancia;
    private int tempoMaximoHoras;
    private float tempoRealHoras;
    private float precoFinal;
    private String nomeCliente;
    private String Veiculo_placa;
    private boolean statusFrete;
    private Conector con= new Conector();
    private ArrayList<Frete> fretes = new ArrayList<>();
    private ArrayList<Veiculo> veiculos = new ArrayList<>();
    private Veiculo seletorVeiculos = new Veiculo();
    public Frete() {
        this.setStatusFrete(true);
    }
    public ArrayList<Frete> getFretes() {
        return fretes;
    }

    public void setFretes(ArrayList<Frete> fretes) {
        this.fretes = fretes;
    }
    public int getIdFrete() {
        return idFrete;
    }

    public void setIdFrete(int idFrete) {
        this.idFrete = idFrete;
    }

    public float getPesoCarga() {
        return pesoCarga;
    }

    public void setPesoCarga(float pesoCarga) {
        this.pesoCarga = pesoCarga;
    }

    public float getDistancia() {
        return distancia;
    }

    public void setDistancia(float distancia) {
        this.distancia = distancia;
    }

    public int getTempoMaximoHoras() {
        return tempoMaximoHoras;
    }

    public void setTempoMaximoHoras(int tempoMaximoHoras) {
        this.tempoMaximoHoras = tempoMaximoHoras;
    }

    public float getTempoRealHoras() {
        return tempoRealHoras;
    }

    public void setTempoRealHoras(float tempoRealHoras) {
        this.tempoRealHoras = tempoRealHoras;
    }

    public float getPrecoFinal() {
        return precoFinal;
    }

    public void setPrecoFinal(float precoFinal) {
        this.precoFinal = precoFinal;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public boolean isStatusFrete() {
        return statusFrete;
    }

    public void setStatusFrete(boolean statusFrete) {
        this.statusFrete = statusFrete;
    }
        public ArrayList<Veiculo> getVeiculos() {
        return veiculos;
    }

    public void setVeiculos(ArrayList<Veiculo> veiculos) {
        this.veiculos = veiculos;
    }

    public String getVeiculo_placa() {
        return Veiculo_placa;
    }

    public void setVeiculo_placa(String Veiculo_placa) {
        this.Veiculo_placa = Veiculo_placa;
    }

    public Frete(int idFrete, float pesoCarga, float distancia, float tempoMaximoHoras, float precoFinal, String nomeCliente, String Veiculo_placa, boolean statusFrete) {
        this.idFrete = idFrete;
        this.pesoCarga = pesoCarga;
        this.distancia = distancia;
        this.tempoRealHoras = tempoMaximoHoras;
        this.precoFinal = precoFinal;
        this.nomeCliente = nomeCliente;
        this.Veiculo_placa = Veiculo_placa;
        this.statusFrete = statusFrete;
    }

    
    public Frete(float pesoCarga, float distancia, int tempoMaximoDias, String nomeCliente) {
        this.pesoCarga = pesoCarga;
        this.distancia = distancia;
        this.tempoMaximoHoras = tempoMaximoDias;
        this.nomeCliente = nomeCliente;
    }

    public Frete(int idFrete, float pesoCarga, float distancia,  float tempoRealHoras, float precoFinal, String nomeCliente, boolean statusFrete) {
        this.idFrete = idFrete;
        this.pesoCarga = pesoCarga;
        this.distancia = distancia;
        this.tempoMaximoHoras = tempoMaximoHoras;
        this.tempoRealHoras = tempoRealHoras;
        this.precoFinal = precoFinal;
        this.nomeCliente = nomeCliente;
        this.statusFrete = statusFrete;
    }
    
    //Construtor && Getter && Setter
    public float calculaPrecoFinal(float margemDeLucro,float precoCombustivel){
        margemDeLucro /=100;
        margemDeLucro =1-margemDeLucro;
        this.precoFinal = precoCombustivel/margemDeLucro;
        return this.precoFinal;
    }
    private void carregarParaMemoriaFretes() throws SQLException{
        fretes.clear();
        try{
            ResultSet fretesNoBanco = con.select("select * from Frete;");
            while(fretesNoBanco.next()){
                int idFrete = fretesNoBanco.getInt("idFrete");
                float pesoCarga = fretesNoBanco.getFloat("pesoCarga");
                float distancia = fretesNoBanco.getFloat("distancia");
                float tempoRealHoras = fretesNoBanco.getFloat("tempoRealHoras");
                float precoFinal = fretesNoBanco.getFloat("precoFinal");
                String nomeCliente = fretesNoBanco.getString("nomeCliente");
                boolean statusFrete = fretesNoBanco.getBoolean("statusFrete");
                String Veiculo_placa = fretesNoBanco.getString("Veiculo_placa");
                fretes.add(new Frete(idFrete,pesoCarga,distancia,tempoRealHoras,precoFinal,nomeCliente,Veiculo_placa,statusFrete));
            }
        }catch(SQLException ex){
            System.out.println("Erro ao carregar fretes "+ ex);
        }
    }
    public ArrayList<Frete> getFretesEmAndamento() throws SQLException{
            try{
            carregarParaMemoriaFretes();
            }catch(SQLException ex){
                System.out.println("Error "+ex);
            }
            ArrayList<Frete> fretesAndamento = new ArrayList<>();
            for(Frete aux : fretes){
                if(aux.isStatusFrete()){
                    fretesAndamento.add(aux);
                }
            }
            return fretesAndamento;
    }
    public boolean finalizarFrete(int idFrete,String placa) throws SQLException{
        try{
            if(con.insert_update_delete("update Frete set statusFrete=false where idFrete="+idFrete+";")){
                if(habilitarVeiculo(placa)){
                    if(atualizaSaldoEmpresa()){// Att. o saldo da empresa.
                        return true;
                    }
                }
                return false;
            }
        }catch(SQLException ex){
            System.out.println("Error "+ex);
        }
        return false;
    }
    private boolean habilitarVeiculo(String placa){
        String query = "update Veiculo set habilitadoParaUso=true where placa='"+placa+"';";
        return con.insert_update_delete(query);
    }
    private boolean desabilitarVeiculo(String placa){
        String query = "update Veiculo set habilitadoParaUso=false where placa='"+placa+"';";
        return con.insert_update_delete(query);
    }
    public boolean adicionarFrete(float tempoMinimo,float margemDeLucro,float precoMelhorCombustivel){
        String nomeCliente = this.nomeCliente;
        float precoFinal = this.calculaPrecoFinal(margemDeLucro, precoMelhorCombustivel);
        float tempoRealHoras = tempoMinimo;
        String Veiculo_placa = seletorVeiculos.getPlaca();
        if(!desabilitarVeiculo(seletorVeiculos.getPlaca()))
            return false;
        return con.insert_update_delete("insert into Frete (Veiculo_placa,pesoCarga,distancia,tempoRealHoras,precoFinal,nomeCliente,statusFrete) values ('"+Veiculo_placa+"',"+ this.pesoCarga+","+this.distancia+","+tempoRealHoras+","+precoFinal+",'"+nomeCliente+"',true);");
    }
    private boolean atualizaSaldoEmpresa() throws SQLException{
        try{
            ResultSet saldoEmpresa = con.select("select lucroTotalDaEmpresa from Empresa where id=1;");
            saldoEmpresa.first();
            double saldo = saldoEmpresa.getDouble("lucroTotalDaEmpresa");
            saldo +=this.getPrecoFinal();
            return con.insert_update_delete("update Empresa set lucroTotalDaEmpresa="+saldo+" where id=1;");
        }catch(SQLException ex){
            System.out.println("Error AttSaldo "+ex);
        }
        return false;
    }

    public Veiculo getSeletorVeiculos() {
        return seletorVeiculos;
    }

    public void setSeletorVeiculos(Veiculo seletorVeiculos) {
        this.seletorVeiculos = seletorVeiculos;
    }
}
