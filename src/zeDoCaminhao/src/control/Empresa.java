/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Conector;
import model.Crud;

/**
 *
 * @author hellsank
 */
public class Empresa {
    private String nome;
    private float margemDeLucro;
    private double lucroTotalDaEmpresa;
    private String senha;
    private ArrayList<Carro> carros = new ArrayList<>();
    private ArrayList<Caminhao> caminhoes = new ArrayList<>();
    private ArrayList<Van> vans = new ArrayList<>();
    private ArrayList<Moto> motos = new ArrayList<>();
    private Conector con = new Conector();
    private Frete frete = null;

    public Frete getFrete() {
        return frete;
    }

    public void setFrete(Frete frete) {
        this.frete = frete;
    }
    public ArrayList<Carro> getCarros() {
        return carros;
    }

    public void setCarros(ArrayList<Carro> carros) {
        this.carros = carros;
    }

    public ArrayList<Caminhao> getCaminhoes() {
        return caminhoes;
    }

    public void setCaminhoes(ArrayList<Caminhao> caminhoes) {
        this.caminhoes = caminhoes;
    }

    public ArrayList<Van> getVans() {
        return vans;
    }

    public void setVans(ArrayList<Van> vans) {
        this.vans = vans;
    }

    public ArrayList<Moto> getMotos() {
        return motos;
    }

    public void setMotos(ArrayList<Moto> motos) {
        this.motos = motos;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getMargemDeLucro() {
        return margemDeLucro;
    }

    public boolean setMargemDeLucro(float margemDeLucro) {
        if(margemDeLucro>=0 && margemDeLucro<100){
            this.margemDeLucro = margemDeLucro;
            return con.insert_update_delete("update Empresa set margemDeLucro='"+margemDeLucro+"'");
        }
        return false;
    }

    public double getLucroTotalDaEmpresa() {
        return lucroTotalDaEmpresa;
    }

    public void setLucroTotalDaEmpresa(double lucroTotalDaEmpresa) {
        this.lucroTotalDaEmpresa = lucroTotalDaEmpresa;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
  
    public Empresa() throws SQLException {
        try{
            this.carregarDadosLevesParaMemoria();
        }catch(SQLException ex){
            System.out.println("Error "+ex);
        }
    }
    //Getter && Setters && Construtor
    public int exibirVeiculos(boolean disponiveis,String tipo){
        this.carregarFrotaParaMemoria();
        int contador = 0;
        if(tipo.equals("Carro")){
            for(Carro aux : carros){
                    if(aux.isHabilitadoParaUso()){
                        contador++;
                    }
            }
            if(!disponiveis){
                contador = carros.size() - contador;
            }
        }else if(tipo.equals("Caminhao")){
            for(Caminhao aux : caminhoes){
                    if(aux.isHabilitadoParaUso()){
                        contador++;
                    }
            }
            if(!disponiveis){
                contador = caminhoes.size() - contador;
            }
        }else if(tipo.equals("Moto")){
            for(Moto aux : motos){
                    if(aux.isHabilitadoParaUso()){
                        contador++;
                    }
            }
            if(!disponiveis){
                contador = motos.size() - contador;
            }
        }else{
            for(Van aux : vans){
                    if(aux.isHabilitadoParaUso()){
                        contador++;
                    }
            }
            if(!disponiveis){
                contador = vans.size() - contador;
            }
        }
        return contador;
    }
    final int CAMINHAO = 0;
    final int CARRO = 1;
    final int MOTO = 2;
    final int VAN = 3;
    public String[] selecionarVeiculoMaisBarato(boolean veiculosDisponiveis[],float distancia,float peso){// Caminhao 0 , Carro 1  Moto 2 Van 3
        this.carregarFrotaParaMemoria();
        String tipoPlacaMaisBarato []= {"void",""};
        float maisBarato = 999999999;
        if(veiculosDisponiveis[CAMINHAO]){
            if(caminhoes.get(0).calculoPrecoMelhorCombustivel(distancia, peso)<maisBarato){
                maisBarato = caminhoes.get(0).calculoPrecoMelhorCombustivel(distancia, peso);
                tipoPlacaMaisBarato[0] = "Caminhao";
                tipoPlacaMaisBarato[1] = caminhoes.get(0).getPlaca();
            }
        }
        if(veiculosDisponiveis[CARRO]){
            if(carros.get(0).calculoPrecoMelhorCombustivel(distancia, peso)<maisBarato){
                maisBarato = carros.get(0).calculoPrecoMelhorCombustivel(distancia, peso);
                tipoPlacaMaisBarato[0] = "Carro";
                tipoPlacaMaisBarato[1] = carros.get(0).getPlaca();
            }
        }
        if(veiculosDisponiveis[MOTO]){
            if(motos.get(0).calculoPrecoMelhorCombustivel(distancia, peso)<maisBarato){
                maisBarato = motos.get(0).calculoPrecoMelhorCombustivel(distancia, peso);
                tipoPlacaMaisBarato[0] = "Moto";
                tipoPlacaMaisBarato[1] = motos.get(0).getPlaca();
            }
        }
        if(veiculosDisponiveis[VAN]){
            if(vans.get(0).calculoPrecoMelhorCombustivel(distancia, peso)<maisBarato){
                maisBarato = vans.get(0).calculoPrecoMelhorCombustivel(distancia, peso);
                tipoPlacaMaisBarato[0] = "Van";
                tipoPlacaMaisBarato[1] = vans.get(0).getPlaca();
            }
        }
        return tipoPlacaMaisBarato;
    }
    public String[] selecionarVeiculoMaisRapido(boolean veiculosDisponiveis[]){
        this.carregarFrotaParaMemoria();
        String tipoPlacaMaisRapido []= {"void",""};
        if(veiculosDisponiveis[MOTO]){
            tipoPlacaMaisRapido[0] = "Moto";
            tipoPlacaMaisRapido[1] = motos.get(0).getPlaca();
        }else if(veiculosDisponiveis[CARRO]){
            tipoPlacaMaisRapido[0] = "Carro";
            tipoPlacaMaisRapido[1] = carros.get(0).getPlaca();
        }else if(veiculosDisponiveis[VAN]){
            tipoPlacaMaisRapido[0] = "Van";
            tipoPlacaMaisRapido[1] = vans.get(0).getPlaca();
        }else if(veiculosDisponiveis[CAMINHAO]){
            tipoPlacaMaisRapido[0] = "Caminhao";
            tipoPlacaMaisRapido[1] = caminhoes.get(0).getPlaca();
        }
        return tipoPlacaMaisRapido;
    }
    public String[] selecionarVeiculoMelhorCustoBeneficio(boolean veiculosDisponiveis[],float distancia,float peso){
        this.carregarFrotaParaMemoria();
        String tipoPlacaCustoBeneficio []= {"void",""};
        float melhorCBen = 999999999;
        if(veiculosDisponiveis[VAN]){
            float tempo_dinheiro = vans.get(0).calculoTempoMinimo(distancia)/vans.get(0).calculoPrecoMelhorCombustivel(distancia, peso);
            if(tempo_dinheiro<melhorCBen){
                tipoPlacaCustoBeneficio[0] = "Van";
                tipoPlacaCustoBeneficio[1] = vans.get(0).getPlaca();
            }
        }
        if(veiculosDisponiveis[CAMINHAO]){
            float tempo_dinheiro = caminhoes.get(0).calculoTempoMinimo(distancia)/caminhoes.get(0).calculoPrecoMelhorCombustivel(distancia, peso);
            if(tempo_dinheiro<melhorCBen){
                tipoPlacaCustoBeneficio[0] = "Caminhao";
                tipoPlacaCustoBeneficio[1] = caminhoes.get(0).getPlaca();
            }
        }
        if(veiculosDisponiveis[CARRO]){
            float tempo_dinheiro = carros.get(0).calculoTempoMinimo(distancia)/carros.get(0).calculoPrecoMelhorCombustivel(distancia, peso);
            if(tempo_dinheiro<melhorCBen){
                tipoPlacaCustoBeneficio[0] = "Carro";
                tipoPlacaCustoBeneficio[1] = carros.get(0).getPlaca();
            }
        }
        if(veiculosDisponiveis[MOTO]){
            float tempo_dinheiro = motos.get(0).calculoTempoMinimo(distancia)/motos.get(0).calculoPrecoMelhorCombustivel(distancia, peso);
            if(tempo_dinheiro<melhorCBen){
                tipoPlacaCustoBeneficio[0] = "Moto";
                tipoPlacaCustoBeneficio[1] = motos.get(0).getPlaca();
            }
        }
        return tipoPlacaCustoBeneficio;
    }
    public boolean validarDados(String nome,String senha) throws SQLException{//Valida os dados na hora de logar
        return (this.nome.equals(nome) && this.senha.equals(senha));
    }
    public boolean validarDados(String nome,String senha,String novaSenha,String confNovaSenha) throws SQLException{
        if(novaSenha.equals(confNovaSenha) && this.senha.equals(senha)){
            String query = "update Empresa set nome='"+nome+"',senha='"+novaSenha+"' where id>'0' limit 1;";
            this.nome = nome;
            this.senha = novaSenha;
            //System.out.println(query);
            return con.insert_update_delete(query);
        }
        return false;
    }
    public boolean adicionarVeiculoNoBanco(String placa, String tipo){
        this.carregarFrotaParaMemoria();
        for(Carro aux :carros){
            if(aux.getPlaca().equals(placa)){
                return false;
            }
        }
        for(Caminhao aux :caminhoes){
            if(aux.getPlaca().equals(placa)){
                return false;
            }
        }
        for(Moto aux :motos){
            if(aux.getPlaca().equals(placa)){
                return false;
            }
        }
        for(Van aux :vans){
            if(aux.getPlaca().equals(placa)){
                return false;
            }
        }
        
        String query = "insert into Veiculo (placa,tipo,habilitadoParaUso,Empresa_id) values ('"+placa+"','"+tipo+"',true,1);";
        return con.insert_update_delete(query);
    }
    public boolean removerVeiculoNoBanco(String placa){
        String query = "delete from  Veiculo where placa='"+placa+"';";
        return con.insert_update_delete(query);
    }
    public boolean preparaFrete(float tempoReal,float precoMelhorCombustivel){
        try {
            this.carregarDadosLevesParaMemoria();
        } catch (SQLException ex) {
            Logger.getLogger(Empresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(this.frete.adicionarFrete(tempoReal,margemDeLucro, precoMelhorCombustivel)){
            return true;
        }else{
            return false;
        }
    }
    //"Passadores" de Dados do BD para a RAM
    private void carregarDadosLevesParaMemoria() throws SQLException{
        String query = "select * from Empresa;";
        ResultSet empresaBanco = con.select(query);
        try{
            empresaBanco.first();
            this.nome =empresaBanco.getString("nome");
            this.senha = empresaBanco.getString("senha");
            this.lucroTotalDaEmpresa = empresaBanco.getDouble("lucroTotalDaEmpresa");
            this.margemDeLucro = empresaBanco.getFloat("margemDeLucro");
            con.fexarConexao(con.getObjetoDeConexao(), con.getResultado(), con.getExecutorQuerys());
            
        }catch(SQLException e){
            System.out.println("Error "+e);
        }
        
    }
    public void carregarFrotaParaMemoria(){
        carros.clear();
        motos.clear();
        caminhoes.clear();
        vans.clear();
        String query = "select * from Veiculo;";
        ResultSet empresaBanco = con.select(query);
        try{
            while(empresaBanco.next()){
                String tipoVeiculo = empresaBanco.getString("tipo");
                String placa = empresaBanco.getString("placa");
                boolean habilitadoParaUso = empresaBanco.getBoolean("habilitadoParaUso");
                if(tipoVeiculo.equals("Carro")){
                    carros.add(new Carro(placa,habilitadoParaUso));
                }else if(tipoVeiculo.equals("Van")){
                    vans.add(new Van(placa,habilitadoParaUso));
                }else if(tipoVeiculo.equals("Caminhao")){
                    caminhoes.add(new Caminhao(placa,habilitadoParaUso));
                }else if(tipoVeiculo.equals("Moto")){
                    motos.add(new Moto(placa,habilitadoParaUso));
                }
            }
            con.fexarConexao(con.getObjetoDeConexao(), con.getResultado(), con.getExecutorQuerys());
            
        }catch(SQLException e){
            System.out.println("Error "+e);
        }
    }
}
