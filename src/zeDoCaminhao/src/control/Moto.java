/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

/**
 *
 * @author hellsank
 */
public class Moto extends Veiculo{
    final short GASOLINA = 0;
    final short ALCOOL = 1;
    Moto(String placa,boolean habilitadoParaUso){
        this();
        this.setPlaca(placa);
        this.setHabilitadoParaUso(habilitadoParaUso);
    }
    public Moto(){
        this.cargaMaxima = 50;
        this.velocidadeMedia = 110;
        float [] aux = {50f,0.3f};//Gasol
        float [] aux1 = {43f,0.4f};//Alcool
        this.combustivelRendimentoReducaoRendimento.add(aux);
        this.combustivelRendimentoReducaoRendimento.add(aux1);
        this.setHabilitadoParaUso(true);
    }
    
    @Override
    public float calculoPrecoMelhorCombustivel(float distancia, float pesoCarga) {
        if(this.calculoPrecoCombustivel(GASOLINA, "GASOLINA", distancia, pesoCarga)< this.calculoPrecoCombustivel(ALCOOL, "ALCOOL", distancia, pesoCarga)){
            return this.calculoPrecoCombustivel(GASOLINA, "GASOLINA", distancia, pesoCarga);
        }else{
            return this.calculoPrecoCombustivel(ALCOOL, "ALCOOL", distancia, pesoCarga);
        }
    }
}
