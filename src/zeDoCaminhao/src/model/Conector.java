/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.Connection;
import control.Empresa;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author hellsank
 */
public class Conector implements Crud{
    private String url;
    private String user;
    private String password;
    private String query;
    private Connection objetoDeConexao;
    private ResultSet resultado;
    private PreparedStatement executorQuerys;
    public Conector(){
        this.setUrl("jdbc:mysql://localhost:3306/euroTruck");
        this.setUser("");
        this.setPassword("");
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    //__________________________________
    private Connection conectar(){
        try{
               Connection con = (Connection) DriverManager.getConnection(url,user,password);
               return con;
           }catch(SQLException e){
               System.out.println("Error "+e);
               return null;
           }
    }
    public void fexarConexao(Connection objetoDeConexao,ResultSet resultado,PreparedStatement executorQuerys) throws SQLException{
        try{
            if(!(resultado==null && executorQuerys==null)){
                resultado.close();
                executorQuerys.close();   
            }else if(resultado==null){
                executorQuerys.close();
            }else if(executorQuerys==null){
                resultado.close();
            }
            objetoDeConexao.close();
        }catch(SQLException e){
            System.out.println("Error "+e);
        }
    }
    //________________________________________
    

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Connection getObjetoDeConexao() {
        return objetoDeConexao;
    }

    public void setObjetoDeConexao(Connection objetoDeConexao) {
        this.objetoDeConexao = objetoDeConexao;
    }

    public ResultSet getResultado() {
        return resultado;
    }

    public void setResultado(ResultSet resultado) {
        this.resultado = resultado;
    }

    public PreparedStatement getExecutorQuerys() {
        return executorQuerys;
    }

    public void setExecutorQuerys(PreparedStatement executorQuerys) {
        this.executorQuerys = executorQuerys;
    }
    @Override
    public ResultSet select(String query) {
        this.setObjetoDeConexao(this.conectar());
        try {
            this.setExecutorQuerys(this.getObjetoDeConexao().prepareStatement(query));
        } catch (SQLException ex) {
            System.out.println("Error "+ex);
        }
        try {
            this.setResultado(this.getExecutorQuerys().executeQuery());
        } catch (SQLException ex) {
            System.out.println("Error "+ex);
        }
        return this.getResultado();
        
    }

    @Override
    public boolean insert_update_delete(String query) {
        this.setObjetoDeConexao(this.conectar());
        try {
            this.setExecutorQuerys(this.getObjetoDeConexao().prepareStatement(query));
        } catch (SQLException ex) {
            System.out.println("Error "+ex);
        }
        try {
            
            return !this.executorQuerys.execute();
        } catch (SQLException ex) {
            System.out.println("Error "+ex);
        }
        try {
            this.fexarConexao(this.getObjetoDeConexao(), null, this.getExecutorQuerys());
        } catch (SQLException ex) {
            Logger.getLogger(Empresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
