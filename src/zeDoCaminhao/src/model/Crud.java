/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;


/**
 *
 * @author hellsank
 */
public interface Crud {
    ResultSet select(String query);
    boolean insert_update_delete(String query);
}
