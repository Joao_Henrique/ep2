/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.Frete;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hellsank
 */
public class TableModelFretes extends AbstractTableModel{
    private Frete freteBase = new Frete();
    public int totalFreteBase(){
        return freteBase.getFretes().size();
    }
    private List<Frete> fretes;
    private String coluna[] = {
      "Peso Carga",
       "Distancia",
       "Preco",
       "Tempo",
       "Nome Cliente"
    };

    public TableModelFretes() throws SQLException {
        try{
        this.fretes = freteBase.getFretesEmAndamento();
        
        }catch(SQLException ex){
            System.out.println("Error "+ex);
        }
    }
    @Override
    public int getRowCount() {
        return fretes.size();
    }

    @Override
    public int getColumnCount() {
        return coluna.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return fretes.get(rowIndex).getPesoCarga();
            case 1:
                return fretes.get(rowIndex).getDistancia();
            case 2:
                return fretes.get(rowIndex).getPrecoFinal();
            case 3:
                return fretes.get(rowIndex).getTempoRealHoras();
            case 4:
                return fretes.get(rowIndex).getNomeCliente();
            case 5:
                return fretes.get(rowIndex).getVeiculo_placa();
            case 6:
                return fretes.get(rowIndex).getIdFrete();
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex){
            case 0:
                return Float.TYPE;
            case 1:
                return Float.TYPE;
            case 2:
                return Float.TYPE;
            case 3:
                return Float.TYPE;
            case 4:
                return String.class;
            case 5:
                return String.class;
            case 6:
                return Integer.TYPE;
        }
        return Object.class;
    }
    

    @Override
    public String getColumnName(int column) {
        return coluna[column];
    }
    public boolean atualizaFretes(int row){
        try {
            return(fretes.get(row).finalizarFrete(fretes.get(row).getIdFrete(),fretes.get(row).getVeiculo_placa()));
        } catch (SQLException ex) {
            Logger.getLogger(TableModelFretes.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
}
