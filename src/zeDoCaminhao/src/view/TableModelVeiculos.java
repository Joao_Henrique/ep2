/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.Veiculo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hellsank
 */
public class TableModelVeiculos extends AbstractTableModel{
    Veiculo veiculo;
    List <Veiculo> veiculos;
    private String colunas[]={
        "Placa",
        "Tipo",
        "Habilitado"
    };

    public TableModelVeiculos() throws SQLException {
        veiculo = new Veiculo();
        try{
            this.veiculos = veiculo.pegaVeiculosNoBanco();
        }catch(SQLException ex){
            System.out.println("Error "+ex);
        }
    }
    @Override
    public int getRowCount() {
        return veiculos.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return veiculos.get(rowIndex).getPlaca();
            case 1:
                return veiculos.get(rowIndex).getTipo();
            case 2:
                return veiculos.get(rowIndex).isHabilitadoParaUso();
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return colunas[column]; //To change body of generated methods, choose Tools | Templates.
    }
    
}
